#!/bin/sh
CMD=python3-coverage
eatmydata $CMD run ./manage.py test "$@" &&
    $CMD html &&
    sensible-browser htmlcov/index.html
