from django.urls import path
from . import views

urlpatterns = [
    path('login/', views.Login.as_view(), name='signon_login'),
    path('logout/', views.Logout.as_view(), name='signon_logout'),
    path('oidc/callback/<name>/', views.OIDCAuthenticationCallbackView.as_view(), name='signon_oidc_callback'),
]
