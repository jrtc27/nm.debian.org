# Local bundling of jwcrypto

This is a copy of jwcrypto 0.6.0-2 from bullseye, bundled locally until a backport is available.

See [#956560](https://bugs.debian.org/956560) to track this.

Sources are at <https://github.com/latchset/jwcrypto>

Debian package is at <https://tracker.debian.org/pkg/python-jwcrypto>

Copyright: 2015 JWCrypto Project Contributors
License: LGPL-3+
