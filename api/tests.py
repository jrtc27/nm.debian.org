from django.test import TestCase
from django.urls import reverse
from backend.unittest import PersonFixtureMixin


class TestStatusAllPermissions(PersonFixtureMixin, TestCase):
    @classmethod
    def __add_extra_tests__(cls):
        # Everyone can see stats
        cls._add_method(cls._test_forbidden, None)
        cls._add_method(cls._test_statusone_success, None)
        for visitor in (
                "pending", "dc", "dc_ga", "dm", "dm_ga", "dd_nu", "dd_u",
                "dd_e", "dd_r", "oldam", "activeam", "fd", "dam"):
            cls._add_method(cls._test_statusone_success, visitor)
            cls._add_method(cls._test_success, visitor)

    def _test_statusone_success(self, visitor):
        client = self.make_test_client(visitor)
        response = client.get(reverse("api_status"), data={"person": "enrico@debian.org"})
        self.assertEqual(response.status_code, 200)

        response = client.post(
                reverse("api_status"),
                data='["enrico@debian.org","example-guest@users.alioth.debian.org"]',
                content_type="application/json")
        self.assertEqual(response.status_code, 200)

    def _test_success(self, visitor):
        client = self.make_test_client(visitor)
        response = client.get(reverse("api_status"))
        self.assertEqual(response.status_code, 200)

        response = client.get(reverse("api_status"), data={"status": "dd_u,dd_nu"})
        self.assertEqual(response.status_code, 200)

    def _test_forbidden(self, visitor):
        client = self.make_test_client(visitor)
        response = client.get(reverse("api_status"))
        self.assertPermissionDenied(response)

        response = client.get(reverse("api_status"), data={"status": "dd_u,dd_nu"})
        self.assertPermissionDenied(response)
