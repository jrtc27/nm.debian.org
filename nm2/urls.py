from django.urls import include, path, re_path
from django.views.generic import TemplateView
from backend.mixins import VisitorTemplateView
from django.conf import settings
from rest_framework import routers
from django.contrib import admin
import django.conf.urls
import backend.views as bviews
import process.views as pviews
import public.views as public_views
import nmlayout.views as nmviews

admin.autodiscover()

router = routers.DefaultRouter()
router.register(r'persons', bviews.PersonViewSet, "persons")
router.register(r'processes', pviews.ProcessViewSet, "processes")

django.conf.urls.handler403 = nmviews.Error403.as_view()
django.conf.urls.handler404 = nmviews.Error404.as_view()
django.conf.urls.handler500 = nmviews.Error500.as_view()


urlpatterns = [
    path('robots.txt', TemplateView.as_view(
        template_name='robots.txt', content_type="text/plain"), name="root_robots_txt"),
    re_path(r'^i18n/', include('django.conf.urls.i18n')),
    re_path(r'^$', VisitorTemplateView.as_view(template_name='index.html'), name="home"),
    re_path(r'^license/$', VisitorTemplateView.as_view(template_name='license.html'), name="root_license"),
    re_path(r'^faq/$', VisitorTemplateView.as_view(template_name='faq.html'), name="root_faq"),
    re_path(r'^members/$', public_views.Members.as_view(), name="members"),
    re_path(r'^legacy/', include("legacy.urls")),
    re_path(r'^person/', include("person.urls")),
    re_path(r'^public/', include("public.urls")),
    re_path(r'^am/', include("restricted.urls")),
    re_path(r'^fprs/', include("fprs.urls")),
    re_path(r'^process/', include("process.urls")),
    re_path(r'^dm/', include("dm.urls")),
    re_path(r'^api/', include("api.urls")),
    re_path(r'^apikeys/', include("apikeys.urls")),
    re_path(r'^keyring/', include("keyring.urls")),
    re_path(r'^wizard/', include("wizard.urls")),
    re_path(r'^mia/', include("mia.urls")),
    re_path(r'^minechangelogs/', include("minechangelogs.urls")),
    re_path(r'^sitechecks/', include("sitechecks.urls")),
    re_path(r'^deploy/', include("deploy.urls")),

    re_path(r'^rest/api/', include(router.urls)),

    path(r'signon/', include("signon.urls")),

    # Uncomment the admin/doc line below to enable admin documentation:
    re_path(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    re_path(r'^admin/', admin.site.urls),
]

if settings.DEBUG:
    try:
        import debug_toolbar
        urlpatterns += [
            re_path(r'^__debug__/', include(debug_toolbar.urls)),
        ]
    except ImportError:
        pass
